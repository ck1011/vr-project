﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playercontrols : MonoBehaviour {
    public float speed;
    public float speedmax;

    private Vector3 initpos = new Vector3(-1, (float)8.8, (float)0.5);

    private void Awake()
    {
        //when scene loaded and object active
    }
    private void FixedUpdate()
    {
        //independent of framerate and pre physics
    }
    private void LateUpdate()
    {
        //called once per frame just before render
    }
    private void OnGUI()
    {
        
    }
    private void OnMouseDown()
    {
        
    }
    private void OnMouseOver()
    {
        
    }
    private void OnCollisionStay(Collision collision)
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        
    }
    // Use this for initialization
    void Start ()
    {
        Debug.Log("playercontrols.cs loaded");
	}

    public void resetpos()
    {
        transform.position = initpos;
        GetComponent<Rigidbody>().velocity = new Vector3();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Debug.Log(GetComponent<Rigidbody>().velocity.magnitude);
        float haxis = speed * Input.GetAxis("Horizontal");
        float vaxis = speed * Input.GetAxis("Vertical");

        if (GetComponent<Rigidbody>().velocity.magnitude < speedmax)
        {
            Vector3 force = new Vector3();
            force += haxis * Camera.main.transform.right;
            force += vaxis * Camera.main.transform.forward;

            GetComponent<Rigidbody>().AddForce(force);
        }
	}
}
