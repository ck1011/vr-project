﻿//#define PCMODE

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WorldControl : MonoBehaviour {
    
    public GameObject cyl;
    public GameObject plyr;

    public float radius = 250;
    
    public float drag = 0.1f;//air drag
    public float gravity = 0.5f;//gravity

    public float forward_accel = 3;
    public float lateral_accel = 3;
    public float vertical_accel = 3;

    public float left_bound = -10;
    public float right_bound = 10;
    public float up_bound = 20;
    public float down_bound = 1;

    public float forward_limit = 10;//speed limit
    public float reverse_limit = 0;
    public float lateral_limit = 10;
    public float vertical_limit = 10;

    private float forward_speed = 0;//current speed
    private float lateral_speed = 0;
    private float vertical_speed = 0;

    public float current_angle = 0;

    public float forward_limit_adjusted = 0;
    
    // Use this for initialization
    void Start()
    {
        forward_limit_adjusted = forward_limit;
    }

    float arc_to_deg(float arclength)
    {
        return (float)(180 * arclength / Mathf.PI / radius);
    }
    
    // Update is called once per frame
    void Update()
    {
        //////////////////PLAYER INPUT
#if (PCMODE)
        float forward_thrust = 1;//= forward_accel * Input.GetAxis("Vertical");
        float vertical_thrust = vertical_accel * Input.GetAxis("Jump");
        float lateral_thrust = lateral_accel * Input.GetAxis("Horizontal");
#else
        float forward_thrust = 1;//= forward_accel * Input.GetAxis("Vertical");
        float vertical_thrust = 0;
        if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
            vertical_thrust = vertical_accel * 1;
        float lateral_thrust = lateral_accel * Input.GetAxis("Jump5");
#endif
        //////////////////////

        //////////forward
        if (forward_thrust != 0)//under accel
        {
            forward_speed += Time.deltaTime * forward_thrust;
        }
        else//no accel
        {
            if (forward_speed > 0)
            {
                forward_speed -= Time.deltaTime * drag;
                if (forward_speed < 0)//crossed 0 
                    forward_speed = 0;
            }
            else if (forward_speed < 0)
            {
                forward_speed += Time.deltaTime * drag;
                if (forward_speed > 0)//crossed 0 
                    forward_speed = 0;
            }
        }
        
        if (forward_speed > forward_limit_adjusted * Time.deltaTime)//enforce limits
            forward_speed = forward_limit_adjusted * Time.deltaTime;
        else if (forward_speed < reverse_limit * Time.deltaTime)
            forward_speed = reverse_limit * Time.deltaTime;

        float rot_deg = arc_to_deg(forward_speed);//convert forward movement into degees
        current_angle += rot_deg;
        cyl.transform.Rotate(rot_deg, 0, 0);
        ////////////////

        ////////////////vertical
        if (vertical_thrust != 0)//under accel
        {
            vertical_speed += Time.deltaTime * vertical_thrust;
        }
        else//no accel
        {
            vertical_speed -= gravity;//always add gravity

            if (vertical_speed > 0)
            {
                vertical_speed -= Time.deltaTime * drag;
                if (vertical_speed < 0)//crossed 0 
                    vertical_speed = 0;
            }
            else if (vertical_speed < 0)
            {
                vertical_speed += Time.deltaTime * drag;
                if (vertical_speed > 0)//crossed 0 
                    vertical_speed = 0;
            }
        }
        
        if (vertical_speed > vertical_limit * Time.deltaTime)//enforce limits
            vertical_speed = vertical_limit * Time.deltaTime;
        else if (vertical_speed < -vertical_limit * Time.deltaTime)
            vertical_speed = -vertical_limit * Time.deltaTime;
        
        if (plyr.transform.position.y < radius + down_bound && vertical_speed < 0)//prevent ground clip
        {
            vertical_speed = 0;//hit ground and speed->0
        }
        else if (plyr.transform.position.y > radius + up_bound && vertical_speed > 0)
        {
            vertical_speed = 0;//hit limit and speed->0
        }
        else
            plyr.transform.Translate(0, vertical_speed, 0);
        //////////////////////

        /////////////////lateral
        if (lateral_thrust != 0)//under accel
        {
            lateral_speed += Time.deltaTime * lateral_thrust;
        }
        else//no accel
        {
            if (lateral_speed > 0)
            {
                lateral_speed -= Time.deltaTime * drag;
                if (lateral_speed < 0)//crossed 0 
                    lateral_speed = 0;
            }
            else if (lateral_speed < 0)
            {
                lateral_speed += Time.deltaTime * drag;
                if (lateral_speed > 0)//crossed 0 
                    lateral_speed = 0;
            }
        }

        if (lateral_speed > lateral_limit * Time.deltaTime)//enforce limits
            lateral_speed = lateral_limit * Time.deltaTime;
        else if (lateral_speed < -lateral_limit * Time.deltaTime)
            lateral_speed = -lateral_limit * Time.deltaTime;

        if (plyr.transform.position.x < left_bound && lateral_speed < 0)//prevent ground clip
        {
            lateral_speed = 0;//hit ground and speed->0
        }
        else if (plyr.transform.position.x > right_bound && lateral_speed > 0)
        {
            lateral_speed = 0;//hit limit and speed->0
        }
        else
            plyr.transform.Translate(lateral_speed, 0, 0);
        //////////////////

        //Debug.Log("Vertical: " + vertical_speed + "  Lateral: " + lateral_speed + "  Forward: " + forward_speed);
    }
}