﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameracontrol : MonoBehaviour {

    public float speed;
    public bool enabled = true;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update()
    {
        if (!enabled)
            return;

        if (Input.GetAxis("Fire1") != 0)
        {
            float mousex = speed * Input.GetAxis("Mouse X");
            float mousey = speed * Input.GetAxis("Mouse Y");
            Vector3 rotatevalue = new Vector3(mousey, mousex * -1, 0);
            transform.eulerAngles = transform.eulerAngles - rotatevalue;
            Debug.Log(mousex);
        }
    }
}