﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camfollow : MonoBehaviour {
    public GameObject target;
    public float interpolationSpeed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 totarget = target.transform.position - transform.position;
        Quaternion angletotarget = Quaternion.LookRotation(totarget);
        transform.rotation = Quaternion.Slerp(transform.rotation, angletotarget, interpolationSpeed);
	}
}
