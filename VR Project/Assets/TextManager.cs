﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextManager : MonoBehaviour {

    public UnityEngine.UI.Text current_text;
    public UnityEngine.UI.Text best_text;

    // Use this for initialization
    void Start ()
    {
		
	}

    private void Awake()
    {
        if (StaticScore.cscore > StaticScore.bscore)
        {
            StaticScore.bscore = StaticScore.cscore;
            best_text.text = "Best: " + (int)StaticScore.bscore;
        }
        if (StaticScore.cscore != 0)
            current_text.text = "Score: " + (int)StaticScore.cscore;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
