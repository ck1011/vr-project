﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGen : MonoBehaviour {
    public GameObject cyl;///to check rotation
    public float radius = 250;
    public float spawn_angle = 20;
    public float skew = 10;

    public GameObject object_1;
    public float objectrate_1;

    public GameObject object_2;
    public float objectrate_2;

    public GameObject object_3;
    public float objectrate_3;

    public GameObject object_4;
    public float objectrate_4;

    public GameObject ring;
    public float ring_separation = 15;

    private float ratesum;

    public float genthreshold = 0.2f;//in degrees    
    private float lastgen = 0;
    private float lastring = 0;
    
    private Queue<GameObject> activeObjects;
    private Queue<GameObject> objectQueue_1;
    private Queue<GameObject> objectQueue_2;
    private Queue<GameObject> objectQueue_3;
    private Queue<GameObject> objectQueue_4;

    private Queue<GameObject> ringQueue;

    private Vector3 preVector;

    // Use this for initialization
    void Start()
    {
        object_1.name = "object_1";
        object_2.name = "object_2";
        object_3.name = "object_3";
        object_4.name = "object_4";
        ring.name = "ring";
        ratesum = objectrate_1 + objectrate_2 + objectrate_3 + objectrate_4;

        //create queues
        activeObjects = new Queue<GameObject>();
        objectQueue_1 = new Queue<GameObject>();
        objectQueue_2 = new Queue<GameObject>();
        objectQueue_3 = new Queue<GameObject>();
        objectQueue_4 = new Queue<GameObject>();
        ringQueue = new Queue<GameObject>();
        
        preVector = new Vector3(0, radius * Mathf.Cos(spawn_angle * Mathf.Deg2Rad), radius * Mathf.Sin(spawn_angle * Mathf.Deg2Rad));

        pre_gen();        
    }

    void pre_gen()
    {
        ////pregen terrain
        for (float angle = genthreshold; angle <= spawn_angle; angle += genthreshold)
        {
            float y = radius * Mathf.Cos(angle * Mathf.Deg2Rad);
            float z = radius * Mathf.Sin(angle * Mathf.Deg2Rad);

            spawn_object(y, z, angle);

            if (angle % ring_separation == 0)
            {
                float r = radius + Random.Range(3, GetComponent<WorldControl>().up_bound - 3);//custom radius to random height
                y = r * Mathf.Cos(angle * Mathf.Deg2Rad);
                z = r * Mathf.Sin(angle * Mathf.Deg2Rad);

                spawn_ring(y, z, angle);
            }
        }

        for (int i = 0; i < 100; i++)//add 100 extra buffer objects
        {
            GameObject obj = Instantiate(object_1, null);
            obj.SetActive(false);
            objectQueue_1.Enqueue(obj);

            obj = Instantiate(object_2, null);
            obj.SetActive(false);
            objectQueue_2.Enqueue(obj);

            obj = Instantiate(object_3, null);
            obj.SetActive(false);
            objectQueue_3.Enqueue(obj);

            obj = Instantiate(object_4, null);
            obj.SetActive(false);
            objectQueue_4.Enqueue(obj);

            if (i % 10 == 0)//add 10 rings
            {
                obj = Instantiate(ring, null);
                obj.SetActive(false);
                ringQueue.Enqueue(obj);
            }
        }
    }

    void spawn_ring(float y, float z, float angle)
    {
        GameObject obj = get_ring();
        obj.SetActive(true);

        obj.transform.localScale = new Vector3(1, 1, 1);

        obj.transform.position = new Vector3(Random.Range(GetComponent<WorldControl>().left_bound + 3, GetComponent<WorldControl>().right_bound - 3), y, z);//within left/right player area

        obj.transform.rotation = new Quaternion(0, 0, 0, 0);
        obj.transform.Rotate(angle, 0, 0);///////fix rotation around relative axis

        obj.transform.parent = cyl.transform;
        activeObjects.Enqueue(obj);
    }

    void spawn_object(float y, float z, float angle)
    {
        for (int i = -1; i <= 1; i += 2)
        {
            GameObject obj = get_object();
            obj.SetActive(true);

            //////size
            float scale = Random.Range(0.8f, 1.2f);
            obj.transform.localScale = new Vector3(scale, scale, scale);

            //////location
            obj.transform.position = new Vector3(Random.Range(20 * i, 100 * i), y, z);

            ///////rotation
            obj.transform.rotation = new Quaternion(0, 0, 0, 0);
            obj.transform.Rotate(angle + Random.Range(-skew, skew), 0, 0);///////fix rotation around relative axis
            obj.transform.RotateAround(obj.transform.up, Random.Range(0, 360));

            obj.transform.parent = cyl.transform;
            activeObjects.Enqueue(obj);
        }
    }

	// Update is called once per frame
	void LateUpdate ()
    {
        //////remove objects
        remove_objects();

        //////add objects
        float angle = GetComponent<WorldControl>().current_angle;
        if (angle > lastgen + genthreshold)
        {
            spawn_object(preVector.y, preVector.z, spawn_angle);
            lastgen = angle;
        }
        if (angle > lastring + ring_separation)
        {
            float r = radius + Random.Range(3, GetComponent<WorldControl>().up_bound - 3);//custom radius to random height
            float y = r * Mathf.Cos(spawn_angle * Mathf.Deg2Rad);
            float z = r * Mathf.Sin(spawn_angle * Mathf.Deg2Rad);
            spawn_ring(y, z, spawn_angle);
            lastring = angle;
        }

        // Debug.Log(activeObjects.Count + " :: " + objectQueue_1.Count + " :: " + objectQueue_2.Count + " :: " + objectQueue_3.Count + " :: " + objectQueue_4.Count + " :: " + ringQueue.Count);
    }

    int count = 0;
    void remove_objects()
    {
        while (true)
        {
            if (activeObjects.Count != 0 && activeObjects.Peek().transform.position.z < -10)
            {
                var temp = activeObjects.Dequeue();
                temp.transform.parent = null;//set to global coord system
                temp.SetActive(false);

                switch (temp.name)
                { 
                    case "object_1(Clone)":
                        objectQueue_1.Enqueue(temp);
                        break;
                    case "object_2(Clone)":
                        objectQueue_2.Enqueue(temp);
                        break;
                    case "object_3(Clone)":
                        objectQueue_3.Enqueue(temp);
                        break;
                    case "object_4(Clone)":
                        objectQueue_4.Enqueue(temp);
                        break;
                    case "ring(Clone)":
                        ringQueue.Enqueue(temp);
                        GetComponent<ScoreManager>().ringcheck();//check if missed ring
                        break;
                    default:
                        Debug.Log("Despawn sort issue " + count++ + "  " + temp.name.ToString());
                        Destroy(temp);
                        break;
                };
            }
            else
                break;
        }
    }

    GameObject clean_object(GameObject obj)
    {
        obj.transform.localScale = new Vector3(0, 0, 0);
        obj.transform.parent = null;

        return obj;
    }

    GameObject get_ring()
    {
        if (ringQueue.Count > 0)
            return clean_object(ringQueue.Dequeue());
        else
            return Instantiate(ring, null);
    }

    GameObject get_object()
    {
        float r = Random.Range(0, ratesum);

        if (r < objectrate_1)
        {
            if (objectQueue_1.Count > 0)
                return clean_object(objectQueue_1.Dequeue());
            else
                return Instantiate(object_1, null);
        }
        else if (r < objectrate_1 + objectrate_2)
        {
            if (objectQueue_2.Count > 0)
                return clean_object(objectQueue_2.Dequeue());
            else
                return Instantiate(object_2, null);
        }
        else if (r < objectrate_1 + objectrate_2 + objectrate_3)
        {
            if (objectQueue_3.Count > 0)
                return clean_object(objectQueue_3.Dequeue());
            else
                return Instantiate(object_3, null);
        }
        else
        {
            if (objectQueue_4.Count > 0)
                return clean_object(objectQueue_4.Dequeue());
            else
                return Instantiate(object_4, null);
        }
    }
}
