﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[Serializable]
public class LookEvent : UnityEvent<GameObject> { }

public class button_delay : MonoBehaviour {
    public LookEvent OnTrigger;

    public float TriggerTime = 5;

    private bool isTriggered = false;
    private float timeTriggered = 0;
    public Image TargetImage;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        // Add to timer
        if (isTriggered)
        {
            timeTriggered += Time.deltaTime;
        }

        
        TargetImage.fillAmount = timeTriggered / TriggerTime;

        // Check timer
        if (timeTriggered >= TriggerTime)
        {
            OnTrigger.Invoke(gameObject);
        }
    }

    public void OnGazeEnter()
    {
        isTriggered = true;
    }

    public void OnGazeExit()
    {
        isTriggered = false;
        timeTriggered = 0;
    }
}
