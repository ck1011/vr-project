﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlTest : MonoBehaviour {

    public UnityEngine.UI.Text text;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        float Horizontal = Input.GetAxis("Horizontal");
        float Vertical = Input.GetAxis("Vertical");
        float Fire1 = Input.GetAxis("Fire1");
        float Fire2 = Input.GetAxis("Fire2");
        float Fire3 = Input.GetAxis("Fire3");
        float Fire4 = Input.GetAxis("Fire4");
        float Fire5 = Input.GetAxis("Fire5");
        float Fire6 = Input.GetAxis("Fire6");
        float Jump = Input.GetAxis("Jump");
        float MouseX = Input.GetAxis("Mouse X");
        float MouseY = Input.GetAxis("Mouse Y");
        float MouseScroll = Input.GetAxis("Mouse ScrollWheel");
        float Submit = Input.GetAxis("Submit");
        float Cancel = Input.GetAxis("Cancel");
        

        if (Horizontal != 0)
            text.text = "Horizontal";
        if (Vertical != 0)
            text.text = "Vertical";
        if (Fire1 != 0)
            text.text = "Fire1";
        if (Fire2 != 0)
            text.text = "Fire2";
        if (Fire3 != 0)
            text.text = "Fire3";
        if (Fire4 != 0)
            text.text = "Fire4";
        if (Fire5 != 0)
            text.text = "Fire5";
        if (Fire6 != 0)
            text.text = "Fire6";
        if (Jump != 0)
            text.text = "Jump";
        if (MouseX != 0)
            text.text = "Mouse X";
        if (MouseY != 0)
            text.text = "Mouse Y";
        if (MouseScroll != 0)
            text.text = "Mouse ScrollWheel";
        if (Submit != 0)
            text.text = "Submit";
        if (Cancel != 0)
            text.text = "Cancel";

        if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger))
            text.text = "Pri H Trig";
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
            text.text = "Pri I Trig";
    }
}