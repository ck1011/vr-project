﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class afterimagemaker : MonoBehaviour {
    public GameObject ghost;
    public float ghosttime = 1;
    public float destroytime = (float)1;
    private float currentime = 0;

    public bool enabled = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        currentime += Time.deltaTime;

        if (currentime > ghosttime)
        {
            if (enabled != true)
                return;

            var ghost1 = Instantiate(ghost, transform.position, transform.rotation);//create new object
            Destroy(ghost1, destroytime);            
            currentime = 0;
        }
	}
}
