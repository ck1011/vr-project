﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour {
    
    private bool hitring = false;

    private int ringmult = 0;
    private int missedrings = 0;
    
    // Use this for initialization
    void Start ()
    {

	}

    private void Awake()
    {
        StaticScore.cscore = 0;
    }

    // Update is called once per frame
    void Update ()
    {
        StaticScore.cscore += Time.deltaTime * 1000 * (ringmult + 1);
	}

    public void ringhit()
    {
        hitring = true;
        ringmult++;
        if (ringmult >= 5)
            ringmult = 5;

        updatespeed();
    }

    public void ringcheck()
    {
        if (hitring == false)
            ringmiss();

        hitring = false;
    }

    void ringmiss()
    {
        ringmult--;
        if (ringmult < 0)
            ringmult = 0;

        missedrings++;

        if (missedrings == 10)
            gameover();

        updatespeed();
    }

    void updatespeed()
    {
        GetComponent<WorldControl>().forward_limit_adjusted = GetComponent<WorldControl>().forward_limit * (float)(1 + 0.2 * ringmult);//from 1x to 2x speed
    }

    void gameover()
    {
        missedrings = 0;
        ringmult = 0;
        updatespeed();//reset speed

        SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
    }
}
