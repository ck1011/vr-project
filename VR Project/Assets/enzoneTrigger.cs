﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//get code from HoleTrigger.cs

[Serializable]
public class AreaEvent : UnityEvent<GameObject> { }

public class enzoneTrigger : MonoBehaviour {

    public AreaEvent OnEnter;
    public AreaEvent OnExit;
    public float TriggerTime = 5;

    private bool istriggered = false;
    private float timetriggered = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (istriggered)
        {
            timetriggered += Time.deltaTime;

            if (timetriggered >= TriggerTime)
            {
                
            }
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        
    }

    private void OnTriggerExit(Collider other)
    {
        
    }
}
