﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WSkybox : MonoBehaviour {

    public GameObject daylight;

    public AnimationCurve sunCurve;

	// Use this for initialization
	void Start () {
		
	}

    private float res = 0.01f;
	// Update is called once per frame
	void LateUpdate () {
        float angle = GetComponent<WorldControl>().current_angle;

        daylight.GetComponent<Light>().intensity = sunCurve.Evaluate(angle % 360) * res;
    }
}
